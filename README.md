# kbdxviewer
* Version: **0.1.14**
* Description: View KeePass2 `.kdbx` database files in various formats and ways
  as XML, CSV or in text/tree representation.
* Using: cryptkeyper/libcx9r from https://github.com/jhagmar/cryptkeyper
* After: https://github.com/luelista/kdbxviewer
* License: GPLv2
* Required for building: libgcrypt-dev libstfl-dev zlib1g-dev libexpat1-dev
* Required for running: libstfl0
* For installation through the git repo: read `INSTALL`
* Or put the binary at `4e4.in/kdbx` in the PATH and install package `libstfl0`

## Usage
```
kdbxviewer 0.1.14 - View KeePass2 .kdbx databases in various formats and ways
Website:  https://gitlab.com/pepa65/kdbxviewer
Usage:  kdbxviewer [command] [option...]
Command, one of:
  -i/--interactive       Interactive viewing: default (no Search argument allowed)
  -t/--text              Output as text (default if Search is used)
  -x/--xml               Output database as XML (no Search allowed)
  -c/--csv               Output as CSV (Search allowed)
  -h/--help              Display this help text
  -V/--version           Display version
Options:
  Search (case-insensitive), one of:
      [-s/--search] STR      Only display entries with STR in the Title
      -S/--searchall STR     Only display entries with STR in any field
  -p/--pasword PW        Decrypt KDBX file using PW (very unsafe!)
  -u/--unmasked          Display password fields unmasked
  -v/--verbose           Verbose (analyze/debug)
  -d/--database KDBX     Use KDBX as the path/filename for the database
Previously used KDBX database path/filenames are stored in file:
 ~/.kdbxviewer  (tried when -d/--database is not given)
```
