// main.c
#define VERSION "0.1.14"
#define CONFIGFILENAME "/.kdbxviewer"
#define PATHLEN 2048

#include <ctype.h>  // for tolower in stristr
#include <getopt.h>  // for long_options
#include <stdio.h>  // for puts/(f)printf/fopen/getline
#include <stdlib.h>  // for exit
#include <unistd.h>  // for getopt
#include <string.h>

#include <cx9r.h>
#include <key_tree.h>
#include "tui.h"
#include "helper.h"

char *search = NULL;
bool searchall = FALSE;
int unmask = 0;

#define BGREEN "\033[1m\033[92m"
#define BRED "\033[1m\033[91m"
#define BYELLOW "\033[1m\033[93m"
#define BCYAN "\033[1m\033[36m"
#define CYAN "\033[36m"
#define YELLOW "\033[33m"
#define DWHITE "\033[47;37m"
#define NORMAL "\033[0m"
#define RESET "\033\143\n"

#define HIDEPW unmask ? "" : DWHITE
#define GROUP BGREEN
#define TITLE BYELLOW
#define FIELD CYAN
#define ERRC BRED
#define PWC YELLOW
#define WARNC BYELLOW

// Display Help
void print_help(char *self, char *configfilename) {
	printf("%s %s - View KeePass2 .kdbx databases in various formats and ways\n", self, VERSION);
	puts("Website:  https://gitlab.com/pepa65/kdbxviewer");
	printf("Usage:  %s [command] [option...]\n", self);
	puts("Command, one of:");
	puts("  -i/--interactive       Interactive viewing: default (no Search allowed)");
	puts("  -t/--text              Output as text (default if Search is used)");
	puts("  -x/--xml               Output database as XML (no Search allowed)");
	puts("  -c/--csv               Output as CSV (Search allowed)");
	puts("  -h/--help              Display this help text");
	puts("  -V/--version           Display version");
	puts("Options:");
	puts("  Search (case-insensitive), one of:");
	puts("      [-s/--search] STR      Only display entries with STR in the Title");
	puts("      -S/--searchall STR     Only display entries with STR in any field");
	puts("  -p/--pasword PW        Decrypt KDBX file using PW (very unsafe!)");
	puts("  -u/--unmasked          Display password fields unmasked");
	puts("  -v/--verbose           Verbose (analyze/debug)");
	puts("  -d/--database KDBX     Use KDBX as the path/filename for the database");
	puts("Previously used KDBX database path/filenames are stored in file:");
	printf(" %s  (tried when -d/--database is not given)\n", configfilename);
}

// Case-insensitive string comparison
char *stristr(const char *haystack, const char *needle) {
	int c = tolower(*needle);
	if(c == '\0') return (char *)haystack;
	for(; *haystack; haystack++) {
		if(tolower(*haystack) == c) {
			for(size_t i = 0;;) {
				if(needle[++i] == '\0') return (char *)haystack;
				if(tolower(haystack[i]) != tolower(needle[i])) break;
			}
		}
	}
	return NULL;
}
int check_filter(cx9r_kt_entry *e, cx9r_kt_group *g) {
	if (search == NULL) return 1;
	if (e->name != NULL && stristr(e->name, search)) return 1;
	if (!searchall) return 0;
	while(g != NULL) {
		if (stristr(cx9r_kt_group_get_name(g), search)) return 1;
		g = cx9r_kt_group_get_parent(g);
	}
	cx9r_kt_field *f = cx9r_kt_entry_get_fields(e);
	while(f != NULL) {
		const char *val = cx9r_kt_field_get_value(f);
		if (val != NULL && stristr(val, search)) return 1;
		f = cx9r_kt_field_get_next(f);
	}
	return 0;
}

// Print Text tree
static void indent(int n) {
	while(n-- > 0) printf("%s|%s ", GROUP, NORMAL);
}
static void dump_tree_field(cx9r_kt_field *f, int depth) {
	if (f->value != NULL) {
		if (strcmp(f->name, "Notes") == 0) indent(depth-1);
		else {
			indent(depth-1);
			printf("%s%s: \"%s", FIELD, f->name, NORMAL);
		}
		if (strcmp(f->name, "Password") == 0)
			printf("%s%s%s", HIDEPW, f->value, NORMAL);
		else printf("%s", f->value);
		if (strcmp(f->name, "Notes") == 0) puts("");
		else printf("%s\"%s\n", FIELD, NORMAL);
	}
	if (f->next != NULL) dump_tree_field(f->next, depth);
}
static void dump_tree_entry(cx9r_kt_group *g, cx9r_kt_entry *e, int depth) {
	if (check_filter(e, g)) {
		indent(depth-1);
		if (e->name != NULL) printf("%s%s%s\n", TITLE, e->name, NORMAL);
		if (e->fields != NULL) dump_tree_field(e->fields, depth);
		else puts("");
	}
	if (e->next != NULL) dump_tree_entry(g, e->next, depth);
}
static void dump_tree_group(cx9r_kt_group *g, int depth) {
	indent(depth);
	if (g->name != NULL) printf("%s%s%s", GROUP, g->name, NORMAL);
	puts("");
	if (g->entries != NULL) dump_tree_entry(g, g->entries, depth + 1);
	if (g->next != NULL) dump_tree_group(g->next, depth);
	if (g->children != NULL) dump_tree_group(g->children, depth + 1);
}

// Print CSV
void print_key_table(cx9r_kt_group *g, int level) {
	cx9r_kt_entry *e = cx9r_kt_group_get_entries(g);
	puts("\"Group\",\"Title\",\"Username\",\"Password\",\"URL\",\"Notes\"");
	while (e != NULL) {
		if (check_filter(e, g)) {
			char *username = dq(getfield(e, "UserName")),
				*password = dq(getfield(e, "Password")),
				*url = dq(getfield(e, "URL")),
				*notes = dq(getfield(e, "Notes"));
			printf("\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
					cx9r_kt_group_get_name(g), cx9r_kt_entry_get_name(e),
					username, password, url, notes);
			// Allocated in helper.c::dq()
			free(username);
			free(password);
			free(url);
			free(notes);
		}
		e = cx9r_kt_entry_get_next(e);
	}
	cx9r_kt_group *c = cx9r_kt_group_get_children(g);
	while(c != NULL) {
		print_key_table(c, level + 1);
		c = cx9r_kt_group_get_next(c);
	}
}

// Process commandline
int main(int argc, char **argv) {
	long unsigned int len = PATHLEN, opt, flags = 0;
	char *kdbxfilename = malloc(len), *filename = malloc(len), command = 0,
		*password = NULL, *self = argv[0] + strlen(argv[0]),
		*configfilename = strcat(getenv("HOME"), CONFIGFILENAME);
	FILE *configfile = NULL, *kdbxfile = NULL;
	*kdbxfilename = 0;

#define abort(code, msg...) do {fprintf(stderr, ERRC); fprintf(stderr, msg); fprintf(stderr, NORMAL); free(kdbxfilename); free(filename); exit(code);} while(0)
#define warn(msg...) do {fprintf(stderr, msg);} while(0)

	static struct option long_options[] = {
		{"xml", no_argument, 0, 'x'},
		{"csv", no_argument, 0, 'c'},
		{"text", no_argument, 0, 't'},
		{"interactive", no_argument, 0, 'i'},
		{"help", no_argument, 0, 'h'},
		{"version", no_argument, 0, 'V'},
		{"verbose", no_argument, 0, 'v'},
		{"password", required_argument, 0, 'p'},
		{"unmasked", no_argument, 0, 'u'},
		{"search", required_argument, 0, 's'},
		{"searchall", required_argument, 0, 'S'},
		{"database", required_argument, 0, 'd'},
		{0, 0, 0, 0}
	};
	static char *short_options = "xctihVvp:us:S:d:";

	while (self >= argv[0] && *self != '/') --self;
	++self;
	int option_index = 0;
	while ((opt = getopt_long(argc, argv, short_options, long_options, &option_index)) != -1) {
		switch (opt) {
		case 'x': flags = 2;
		case 'c':
		case 't':
		case 'i':
		case 'h':
		case 'V':
			if (command != 0) abort(-1, "Multiple commands not allowed\n");
			command = opt;
			break;
		case 'v':
			// extern int from include/cx9r.h
			g_enable_verbose = 1;
			break;
		case 'u':
			unmask = 1;
			break;
		case 'p':
			password = optarg;
			break;
		case 'S':
			searchall = TRUE;
		case 's':
			if (search != NULL)
				abort(-2, "Superfluous search term: %s\n", optarg);
			search = optarg;
			break;
		case 'd':
			if ((kdbxfile = fopen(optarg, "r")) == NULL)
				abort(-3, "Can't open database file: %s\n", optarg);
			kdbxfilename = optarg;
			break;
		default:
			abort(-4, "Unrecognized argument -%s\n", argv[optind]);
		}
	}

	if (command == 'h') {
		print_help(self, configfilename);
		return 0;
	}
	if (command == 'V') {
		printf("%s %s\n", self, VERSION);
		return 0;
	}

	if (optind < argc) // Must be [-s] argument, unless already given
		if (search == NULL) search = argv[optind++];
		else abort(-5, "Superfluous argument: %s\n", argv[optind]);

	if (optind < argc)
		abort(-6, "Superfluous argument: %s\n", argv[optind]);

	if (*kdbxfilename == 0) { // Try configfile for database filename
		*filename = 0;
		if ((configfile = fopen(configfilename, "r")) != NULL)
			while (getline(&filename, &len, configfile) != -1) {
				*(filename+strlen(filename)-1) = 0;
				// Check the latest found file
				if ((kdbxfile = fopen(filename, "r")) != NULL) strcpy(kdbxfilename, filename);
				*filename = 0;
			}
		if (*kdbxfilename == 0)
			abort(-7, "No database specified on commandline or in configfile\n");
		else strcpy(filename, kdbxfilename);
	}

	// Set default mode depending on search
	if (search != NULL) { // Search requested
		if (command == 'i' || command == 'x')
			abort(-8, "Cannot Search with -i/--interactive or -x/--xml\n");
		if (command == 0) command = 't';
	} else { // No search
		if (command == 0) command = 'i';
	}

	// Open the database
	warn("%skdbxviewer", NORMAL);
	if (password == NULL) {
		warn(" - Opening KDBX database %s%s\n%sPassword: %s", FIELD, kdbxfilename, PWC, NORMAL);
		password = getpass("");
	}
	warn("\n");
	cx9r_key_tree *kt = NULL;
	cx9r_err err = cx9r_kdbx_read(kdbxfile, password, flags, &kt);
	if (!err) {
		if ((configfile = fopen(configfilename, "a")) == NULL)
			warn("%sCan't write to configfile %s%s\n", WARNC, configfilename, NORMAL);
		else if (strcmp(filename, kdbxfilename) != 0)
			fprintf(configfile, "%s\n", kdbxfilename);
		if (command == 't') dump_tree_group(&kt->root, 0);
		if (command == 'c') print_key_table(cx9r_key_tree_get_root(kt), 0);
		if (command == 'i') run_interactive_mode(kdbxfilename, kt);
	}
	else {
		warn(WARNC);
		// See include/cx9r.h for error codes
		if (err == 3 || err == 16) warn("Password invalid\n");
		else if (err == CX9R_BAD_MAGIC)
			warn("Not a KeePass databases\n");
		else if (err == CX9R_UNSUPPORTED_VERSION)
			warn("Unsupported KeePass database version\n");
		else if (err == CX9R_FILE_READ_ERR)
			warn("Error reading KeePass database\n");
		else warn("KeePass Database error %d\n", err);
		warn(NORMAL);
	}
	if (kt != NULL) cx9r_key_tree_free(kt);
	return err;
}
